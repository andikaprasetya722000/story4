from django.test import TestCase, Client
from django.apps import apps
from . apps import Story8Config
from .views import fungsi_data, fungsi_suatu_url

# Create your tests here.
class Tests(TestCase):
    def test_url_resolved(self):
        response = Client().get('/story8/')
        self.assertEquals(response.status_code, 200)
    def test_url_resolved2(self):
        response = Client().get('/data/?q=')
        self.assertEquals(response.status_code, 200)

class TestApp(TestCase):
    def testConfig(self):
        self.assertEqual(Story8Config.name, 'story8')