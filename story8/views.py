from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
import requests
import json

# Create your views here.
def fungsi_suatu_url(request):
    response = {}
    return render(request, 'search.html', response)

def fungsi_data(request):
    arg = request.GET['q']
    url_tujuan = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    r = requests.get(url_tujuan)

    data = json.loads(r.content)
    return JsonResponse(data, safe=False)