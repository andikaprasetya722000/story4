from django.test import TestCase, Client
from django.urls import reverse, resolve
from.models import Kegiatan,Peserta
from . import views
from . import models
# Create your tests here.
class story6test(TestCase):

    def setUp(self):
        Kegiatan.objects.create(nama_kegiatan="sehat")
        Peserta.objects.create(nama_peserta="dika",kegiatan=Kegiatan.objects.get(nama_kegiatan="sehat"))

    def test_list_url_resolve(self):
        response = Client().get('/kegiatan/')
        self.assertEquals(response.status_code, 200)

    def test_list_url_resolve2(self):
        response = Client().get('/kegiatan/tambahkegiatan')
        self.assertEquals(response.status_code, 200)

    def test_list_url_resolve3(self):
        response = Client().get('/kegiatan/tambahpeserta')
        self.assertEquals(response.status_code, 200)



    