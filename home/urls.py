from django.urls import path

from . import views
from django.urls import path, include

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('index', views.index, name='index'),
    path('home', views.home, name='home'),
    path('bio', views.bio, name='bio'),
    path('socmed', views.socmed, name='socmed'),
    path('studies', views.studies, name='studies'),
    path('story1', views.story1, name='story1'),
]