from django.shortcuts import render, redirect

# Create your views here.
def index(request):
    return render(request, 'home/index.html')

def home(request):
    return render(request, 'home/home.html')

def bio(request):
    return render(request, 'home/bio.html')

def socmed(request):
    return render(request, 'home/socmed.html')

def studies(request):
    return render(request, 'home/studies.html')

def story1(request):
    return render(request, 'home/index.html')
