from django.db import models

# Create your models here.
class Jadwal(models.Model):
    matkul = models.CharField(max_length=120)
    dosen = models.CharField(max_length=120)
    sks = models.CharField(max_length=30)
    deskripsi = models.CharField(max_length=200)
    semester = models.CharField(max_length=20)
    ruang = models.CharField(max_length=20)
