from django.shortcuts import render, redirect

from .models import Jadwal
# Create your views here.

def add(request):
    param = False
    
    sdata = Jadwal.objects.all()

    if request.method == "POST" :
        param = True 
        matakuliah = request.POST.get("nama")
        semesterv = request.POST.get("semester")
        sksv = request.POST.get("sks")
        dosenv = request.POST.get("dosen")
        ruangv = request.POST.get("ruang")
        deskripsiv = request.POST.get("deskripsi")
        p = Jadwal.objects.create(matkul = matakuliah, dosen = dosenv, sks = sksv, deskripsi = deskripsiv, semester = semesterv, ruang = ruangv)
        p.save()

    context = {
        'par' : param,
        'send': sdata
    }
    return render(request, 'jadwal.html',context)

def delete(request, pk):
    item=Jadwal.objects.get(id=pk)
    if request.method == "POST":
        item.delete()
        return redirect("/schedule")
    context ={
        'matkulv': item,
    }
    return render(request,'remove.html',context)
