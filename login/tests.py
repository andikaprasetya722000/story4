from django.test import TestCase, Client, RequestFactory
from django.urls import reverse, resolve
from . import views
from . import forms
from .apps import LoginConfig
from django.contrib.auth.models import User, AnonymousUser
from django.contrib import auth

# Create your tests here.
class Tests(TestCase):
    def setUp(self):
        self.register_url=reverse('register')
        self.login_url=reverse('login')
        self.user={
            'email':'testemail@gmail.com',
            'username':'username',
            'password1':'password',
            'password2':'password',
        }
        User.objects.create_user(username="username",email="nanana@gmail.com",password="password")


    def test_url_resolved(self):
        response = Client().get('/register/')
        self.assertEquals(response.status_code, 200)
    def test_url_resolved2(self):
        response = Client().get('/login/')
        self.assertEquals(response.status_code, 200)

        
    def test_login_success(self):
        self.client.post(self.register_url,self.user,format='text/html')
        user=User.objects.filter(email=self.user['email']).first()
        response= self.client.post(self.login_url,self.user,format='text/html')
        self.assertEquals(response.status_code, 200)

    def test_login_form_redirect(self):
        self.client.logout()

        response = self.client.get(reverse('login'))
        self.assertEqual(response.status_code, 200)

        self.assertTrue(self.client.login(username='username', password='password'))
        response = self.client.get(reverse('login'))
        self.assertRedirects(response, expected_url=('/'), status_code=302, target_status_code=200)
        
    def test_logout_success(self):
        response = Client().get('/logout/')
        self.assertEquals(response.status_code, 302)

    def test_view_login(self):
        found = resolve('/login/')
        self.assertEqual(found.func, views.loginPage)


class TestApp(TestCase):
    def testConfig(self):
        self.assertEqual(LoginConfig.name, 'Story9')
