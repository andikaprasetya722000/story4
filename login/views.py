from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .forms import RegisterForm

# Create your views here.
def register(request):
    if request.user.is_authenticated:
        return redirect('/')
    else:
        form = RegisterForm()
        if request.method == "POST":
            form = RegisterForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, 'Account was created for ' + user)
                return redirect("/login/")
            
        return render(request, "register.html", {"form":form})

def loginPage(request):
    if request.user.is_authenticated:
        return redirect('/')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('/landing/')
            else:
                return render(request,'login.html', {
                    'error_message': ' Login Failed! Enter the username and password correctly', })

        context = {'number':8}
        return render(request, 'login.html', context)

def logoutUser(request):
    logout(request)
    return redirect('/login/')

def landing(request):
    return render(request, 'landing.html')
