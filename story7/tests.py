from django.test import TestCase
from . import views
from . apps import Story7Config

# # Create your tests here.

class story7(TestCase):

    def test_page(self):
        response = Client().get("/story7/")
        self.assertEqual(response.status_code,200)
    
    def testConfig(self):
        self.assertEqual(Story7Config.name, 'story7')
    