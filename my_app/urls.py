"""my_app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from story8.views import fungsi_suatu_url, fungsi_data
from login import views as s

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('home.urls')),
    path('schedule', include('schedule.urls')),
    path('kegiatan/',include('kegiatan.urls',namespace = 'templates')),
    path('story7/', include(('story7.urls','story7'), namespace = 'story7')),
    path('story8/', fungsi_suatu_url),
    path('data/', fungsi_data),
    path('login/', s.loginPage, name='login'),
    path('logout/', s.logoutUser, name='logout'),
    path('register/', s.register, name='register'),
    path('landing/', s.landing, name='landing'),
    path('', include("django.contrib.auth.urls")),
]
